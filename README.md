# GitLab Runner

## Bootstrap GitLab Runner on host

Run the following to initially setup the GitLab runner. Choose which fits your
setup best. If unsure, use [Docker Compose](#docker-compose).

### Docker Compose

```shell
git clone https://gitlab.com/just-ci/tools/gitlab-runner.git
(cd gitlab-runner && docker compose up -d)
rm -rf gitlab-runner
```

### Docker Swarm

Ensure your Docker daemon is setup as a Docker swarm manager using
`docker swarm init`.

```shell
git clone https://gitlab.com/just-ci/tools/gitlab-runner.git
(cd gitlab-runner && docker stack deploy -c docker-compose.yml gitlab-runner)
rm -rf gitlab-runner
```

## Add runner

Now we need to register a project or group with the runner, by running this on
the host where you just added the runner. This can be run interactively:

```shell
. <(curl -sSLf "https://gitlab.com/just-ci/tools/gitlab-runner/-/raw/v1.2.3/register.sh")
```

### Privileged

This can be useful when you want to deploy containers on that host using the
runner. Make sure only trusted parties get access to this runner.

Add `/var/run/docker.sock:/var/run/docker.sock` to the volumes key in
`config.toml` to give each job access to the deamon.

You can edit this file in `vi` using the following command:

```shell
docker exec -ti gitlab-runner vi /etc/gitlab-runner/config.toml
```

Or just paste the following in your shell:

```shell
docker exec -ti 5e sh -c "sed -i 's|/cache\"|/cache\", \"/var/run/docker.sock:/var/run/docker.sock\"|g' /etc/gitlab-runner/config.toml"
```

> This will update _all_ runners in that config, so make sure you want that.

## Keeping runner up to date

Create a GitLab project which has access to the runner, which also is privileged
as per the previous paragraph. Then add this to the project's `.gitlab-ci.yml`
depending on your setup.

### Docker Compose

```yaml
---
include:
  remote: https://gitlab.com/just-ci/tools/gitlab-runner/-/raw/v1.2.3/update.yml
```

### Docker Swarm

```yaml
---
include:
  remote: https://gitlab.com/just-ci/tools/gitlab-runner/-/raw/v1.2.3/update-swarm.yml
```

Finally [schedule](https://docs.gitlab.com/ee/ci/pipelines/schedules.html) the
job in GitLab.

### Optional override values

If you need to override the default `docker-compose.yml`, create a
`docker-compose.override.yml` in the same repo and add your changes there before
running the deploy command. The update job will ensure this file is taken into
account as well.
