## [1.2.3](https://gitlab.com/just-ci/tools/gitlab-runner/compare/v1.2.2...v1.2.3) (2023-10-28)


### Bug Fixes

* further increase sleep to prevent timeouts ([e006603](https://gitlab.com/just-ci/tools/gitlab-runner/commit/e006603511662c2cce3d7bb196f64ec2920d5a32))

## [1.2.2](https://gitlab.com/just-ci/tools/gitlab-runner/compare/v1.2.1...v1.2.2) (2023-04-01)


### Bug Fixes

* increase sleep ([23659a1](https://gitlab.com/just-ci/tools/gitlab-runner/commit/23659a1dfff98efd0ce2b1ec5223fa6a197328f1))

## [1.2.1](https://gitlab.com/just-ci/tools/gitlab-runner/compare/v1.2.0...v1.2.1) (2023-02-27)


### Bug Fixes

* add sleep to allow job to finish ([10befa0](https://gitlab.com/just-ci/tools/gitlab-runner/commit/10befa0dd79ab29367b29b982cb5bc91d644c50d))

# [1.2.0](https://gitlab.com/just-ci/tools/gitlab-runner/compare/v1.1.6...v1.2.0) (2023-02-05)


### Features

* run in separate container ([b9ec33e](https://gitlab.com/just-ci/tools/gitlab-runner/commit/b9ec33e392b179414fb800cdf7dea7211020d1ed))

## [1.1.6](https://gitlab.com/just-ci/tools/gitlab-runner/compare/v1.1.5...v1.1.6) (2023-02-05)


### Bug Fixes

* simpler ([b687768](https://gitlab.com/just-ci/tools/gitlab-runner/commit/b687768a38fd85383f3111f3148ddd267ad4cec0))

## [1.1.5](https://gitlab.com/just-ci/tools/gitlab-runner/compare/v1.1.4...v1.1.5) (2023-01-24)


### Bug Fixes

* revert silly change ([761f848](https://gitlab.com/just-ci/tools/gitlab-runner/commit/761f848fec9df4cac9ef9b22dde1b169f3b78b06))

## [1.1.4](https://gitlab.com/just-ci/tools/gitlab-runner/compare/v1.1.3...v1.1.4) (2023-01-21)


### Bug Fixes

* detach to prevent job timing out ([52e4350](https://gitlab.com/just-ci/tools/gitlab-runner/commit/52e4350213b60d7e65224a99eee6549140187045))

## [1.1.3](https://gitlab.com/just-ci/tools/gitlab-runner/compare/v1.1.2...v1.1.3) (2023-01-12)


### Bug Fixes

* don't expose daemon to runner by default ([a6f238b](https://gitlab.com/just-ci/tools/gitlab-runner/commit/a6f238b50009b69db06ec7f49287454b356a937d))

## [1.1.2](https://gitlab.com/just-ci/tools/gitlab-runner/compare/v1.1.1...v1.1.2) (2022-12-23)


### Bug Fixes

* quiet update ([80e26dc](https://gitlab.com/just-ci/tools/gitlab-runner/commit/80e26dce37038ffc9057d3e4fb26565aabc0d8b2))

## [1.1.1](https://gitlab.com/just-ci/tools/gitlab-runner/compare/v1.1.0...v1.1.1) (2022-11-15)


### Bug Fixes

* use gitlab's own registry ([2abb42a](https://gitlab.com/just-ci/tools/gitlab-runner/commit/2abb42af642d56c444d50bca867b34bf91c22ed1))

# [1.1.0](https://gitlab.com/just-ci/tools/gitlab-runner/compare/v1.0.1...v1.1.0) (2022-07-25)


### Features

* swarm runner ([c8e7851](https://gitlab.com/just-ci/tools/gitlab-runner/commit/c8e7851ac7662856a1483ca2e97791e5ad6d1762))

## [1.0.1](https://gitlab.com/just-ci/tools/gitlab-runner/compare/v1.0.0...v1.0.1) (2022-07-23)


### Bug Fixes

* send to background ([6199cae](https://gitlab.com/just-ci/tools/gitlab-runner/commit/6199cae0c99d172e1183c45392c1b6575aed02f4))

# 1.0.0 (2022-07-23)


### Bug Fixes

* do proper check if running ([e972fec](https://gitlab.com/just-ci/tools/gitlab-runner/commit/e972fec809d6c5cd2fa34f71ffc9d03f5363c187))
* dont exit shell on failure ([71473de](https://gitlab.com/just-ci/tools/gitlab-runner/commit/71473deda525023cc2f08e5acf69539c513d3cde))
* first download base ([1287bec](https://gitlab.com/just-ci/tools/gitlab-runner/commit/1287bec98e7541eb20b10d1f01ee3543bfe43ef7))


### Features

* allow custom docker compose ([093163f](https://gitlab.com/just-ci/tools/gitlab-runner/commit/093163fec997c3d098771938c262f9e14d52f499))
* allow override ([c5dcf23](https://gitlab.com/just-ci/tools/gitlab-runner/commit/c5dcf238182e6b6b0276cc2550f6811e14a56385))
* use basic compose ([a732c87](https://gitlab.com/just-ci/tools/gitlab-runner/commit/a732c8754d273fa7244df23c41ff23bab0498f97))
