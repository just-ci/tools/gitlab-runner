#!/usr/bin/env sh

if ! docker ps >/dev/null; then
  echo "[!] Unable to interact with Docker daemon. Try running this in a root shell."
else
  RUNNER_CONTAINER_COMPOSE=$(docker ps --filter "label=com.docker.compose.service=gitlab-runner" -q)
  RUNNER_CONTAINER_SWARM=$(docker ps --filter "label=com.docker.stack.namespace=gitlab-runner" -q)

  if [ "${RUNNER_CONTAINER_COMPOSE}" != "" ]; then
    echo "[+] Using Docker Compose container id ${RUNNER_CONTAINER_COMPOSE}"
    RUNNER_CONTAINER=${RUNNER_CONTAINER_COMPOSE}
  elif [ "${RUNNER_CONTAINER_SWARM}" != "" ]; then
    echo "[+] Using Docker Swarm container id ${RUNNER_CONTAINER_SWARM}"
    RUNNER_CONTAINER=${RUNNER_CONTAINER_SWARM}
  else
    echo "[!] Container not found. Perhaps wait a bit until it is up?"
    exit 1
  fi

  if ! [ "$(docker container inspect -f '{{.State.Status}}' ${RUNNER_CONTAINER})" = "running" ]; then
      echo "[!] GitLab Runner container not running. See README: https://gitlab.com/just-ci/tools/gitlab-runner."
  else
    export CI_SERVER_URL="https://gitlab.com/"
    export RUNNER_EXECUTOR="docker"
    export DOCKER_IMAGE="docker:latest"
    export RUNNER_NAME="Docker runner on $(hostname)"

    docker exec -ti -e CI_SERVER_URL -e RUNNER_EXECUTOR -e DOCKER_IMAGE -e RUNNER_NAME "${RUNNER_CONTAINER}" gitlab-runner register
  fi
fi
